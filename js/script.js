/* ====================== DARK THEME ====================== */
const themeButton = document.getElementById("theme-button")
const darkTheme = "dark-theme"
const iconTheme = "uil-sun"

// PREVIOUSLY SELECTED TOPIC
const selectedTheme = localStorage.getItem("selected-theme")
const selectedIcon = localStorage.getItem("selected-icon")

// Obtain the current theme that the interface has by validating the dark theme class
const getCurrentTheme = () => document.body.classList.contains(darkTheme) ? "dark" : "light"
const getCurrentIcon = () => document.body.classList.contains(iconTheme) ? "uil-moon" : "uil-sun"

// Need to validate if the user has previously chosen a topic
if(selectedTheme) {
    document.body.classList[selectedTheme === "dark" ? "add" : "remove"](darkTheme)
    document.body.classList[selectedIcon === "uil-moon" ? "add" : "remove"](iconTheme)
}

// Activate / deactivate the theme manually with the button
themeButton.addEventListener('click', () => {
    // Add or remove the dark/light icon -- icon theme
    document.body.classList.toggle(darkTheme)
    themeButton.classList.toggle(iconTheme)
    localStorage.setItem("selected-theme", getCurrentTheme())
    localStorage.setItem("selected-icon", getCurrentIcon())
})

/* ====================== MENU SHOW & HIDE ====================== */
const navMenu = document.getElementById("nav-menu")
const navToggle = document.getElementById("nav-toggle")
const navClose = document.getElementById("nav-close")

// SHOW MENU
if(navToggle) {
    navToggle.addEventListener('click', () => {
        navMenu.classList.add("show-menu")
    })
}

// HIDE MENU
if(navClose) {
    navClose.addEventListener('click', () => {
        navMenu.classList.remove("show-menu")
    })
}

// REMOVE MENU PROFILE ON MENU LINK TAP
const navLink = document.querySelectorAll(".nav__link")
function linkAction() {
    const navMenu = document.getElementById("nav-menu")
    navMenu.classList.remove('show-menu')
}

navLink.forEach(n => n.addEventListener('click', linkAction))

/* ====================== Typewriter Effect ====================== */
new Typewriter('#typewriter', {
    strings: ["Andrei Stoia", "iOS Software Engineer", "@Revolut, Fitbit Alumni"],
    autoStart: true,
    loop: true,
    cursor: "|"
});

/* ====================== Portfolio Swiper Effect ====================== */
var swiper = new Swiper(".blog-slider", {
    spaceBetween: 30,
    effect: 'fade',
    loop: true,
    mousewheel: {
        invert: false,
    },
    // navigation: {
    //   nextEl: ".swiper-button-next",
    //   prevEl: ".swiper-button-prev",
    // },
    pagination: {
      el: ".blog-slider__pagination",
      clickable: true,
    },
    keyboard: true,
  });

/* ====================== SCROLL UP ====================== */
function scrollUp() {
    const scrollup = document.getElementById("scroll-up")
    // When the scroll is higher than 560 viewpoint /height,
    // then the scroll up icon should appear and on click should reach top of the page
    if(this.scrollY >= 560) {
        scrollup.classList.add("show-scroll")
    } else {
        scrollup.classList.remove("show-scroll")
    }
}

window.addEventListener('scroll', scrollUp)

/* ====================== SCROLL ACTIVE SECTION HIGHLIGHT ====================== */
const sections = document.querySelectorAll("section[id]")

function scrollActive() {
    const scrollY = window.pageYOffset
    sections.forEach(current => {
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop - 50
        const sectionBottom = sectionTop + sectionHeight
        const sectionId = current.getAttribute('id')

        if(scrollY > sectionTop && scrollY <= sectionBottom) {
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.add("active-link")
        } else {
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.remove("active-link")
        }
    })
}

window.addEventListener('scroll', scrollActive)